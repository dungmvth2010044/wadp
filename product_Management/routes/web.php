<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\StockController;

/*
Routing cho web
| ------------------------------------------------- -------------------------
| Các tuyến web
| ------------------------------------------------- -------------------------
|
| Đây là nơi bạn có thể đăng ký các tuyến web cho ứng dụng của mình. Này
| các tuyến đường được tải bởi RouteServiceProvider trong một nhóm mà
| chứa nhóm phần mềm trung gian "web". Bây giờ tạo ra một cái gì đó tuyệt vời!
|
*/ 

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function() {
    return redirect('stocks');

});

Route::resource('stocks', StockController::class);
