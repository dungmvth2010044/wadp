<?php
    //Array
    $empployees = array("D", "H", "F");//index: 0-2

    echo $empployees[0]."<br>";//D

    $ages = array("D" => "18", "H" => "19", "F" => "20");
    
    echo "Tuoi cua D la: ". $ages["D"]."<br>"; //$ages["0"]


    //Dem so phan tu trong Array (count() , sizeof())
    $top_cars =["Iphone", "Samsung", "Vertu", "Oppo"];
    echo "So phan tu trong mang la : ".count($top_cars)."<br>";


    //Xoa mang

    $top_car1 =["Toyota", "Honda", "BMW", "Ford", "Hyundai", "Mercedes"];
    unset($top_car1[5]);
    print_r($top_car1)."<br>";


    // Array 2 chieu:
    $cars = array(
                array("Bmw",5,2),

                array("Audi",10,2),
    
            );
    echo $cars[0][0]. " In inventory: ".$cars[0][1].", sold: ".$cars [0][2]."<br>";
    echo $cars[1][0]. " In inventory: ".$cars[1][1].", sold: ".$cars [1][2]."<br>";
    
    //Array and Function

    $ary = array("Hai" => "18", "Minh" => "19", "Hong" => "25");
    $ary1 = array("Nam" => "29", "H Anh" => "19", "Bao" => "25");

    //array_merge() ket hop 2 mang lai voi nhau

    print_r(array_merge($ary,$ary1))."<br>";//ket hop mang
    print_r($ary)."<br>"; //ham buildin
    
    //array_unique() loai tru gia tri trung nhau
    $result = array_unique($ary);//tra ve gia tri ko trung nhau
    print_r($result);

    sort($ary);//tang dan
    print_r($ary)."<br>";

    rsort($ary);//sap xep giam dan
    print_r($ary)."<br>";
    
    //print_r() : danh so thu tu va in mang ra

    asort($ary1);//sap xep ket hop voi key
    print_r($ary1)."<br>";
?>