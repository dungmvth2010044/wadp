<?php
    //Data type:
    //1.primitive data type : boolean, integer, float, string
    //2.reference data type : array, object

    $s = "This is PHP data type page ";//string
    echo $s."<br>";

    $f = 1.5;//float
    echo $f."<br>";
        
    $i = 10;//integer
    $concat_i = $i."\n conver integer"; //integer
    echo $concat_i."<br>";

    $a = array("Toyota", "Honda", "BMW");//array
    print_r($a)."<br>";

?>