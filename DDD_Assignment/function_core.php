<?php
     //Function

    //Trien khai ham
    function greeting($message){
        echo $message;
    }

    //goi ham
    greeting("Hello world ");

    function sum($num1, $num2)
    {
        $result = $num1 + $num2;
        echo "Sum is: ".$result."<br>";

    }

    sum(30,50);

    //gan gia tri cho tham so ham
    function setAge($age = 18){
        echo "Your age is: $age";
    }
    setAge(30);
    echo "<br>";
    setAge();
    echo "<br>";

    //return values
    function add($x, $y){
        $z = $x + $y;
        return $z;
    }

    echo "The sum is: ".add(6,12);//goi ham

?>